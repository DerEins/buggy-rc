#include <Arduino.h>

#include "include/Buggy.h" // voir doc/Buggy.md
#include "include/Roue.h" // voir doc/Roue.md
#include "include/Moteur.h" // voir doc/Moteur.md
#include "include/Radio.h" // voir doc/Radio.md

Roue RAvG(4); // initialisation des 4 roues
Roue RAvD(5);
Roue RArG(6);
Roue RArD(7);
Frein FArG(0); // initialisation des 2 freins
Frein FArD(1);
Frein Both(3);
Moteur Moteur(2); // initialisation du moteur
Radio Radio(A0); // initialisation de la radio
Buggy Buggy(20,3,RAvG,RAvD,RArG,RArD,FArG,FArD,Both,Moteur); // initialisation du buggy
bool freinG(false);
bool freinD(false);
int diffsignal =0;
void setup() {

        for (int i(0); i < 3; i++) // initialisation des sorties numériques
        {
                pinMode(i, OUTPUT);
        }
        for (int j(4); j < 8; j++) // initialisation des entrées numériques
        {
                pinMode(j, INPUT);
        }
        pinMode(A0, INPUT); // initialisation des entrées analogiques

        Serial.begin(9600); // initialisation du moniteur série à 9600 bauds
}

// Boucle principale du programmme, qui tourne à l'infini
void loop()
{
        if (Buggy.glissRel() > 130)
        {
          
            freinD = false;
            FArD.freiner(freinD);
            freinG = true;
            FArG.freiner(freinG);
        }
        
        else if (Buggy.glissRel() < 75)
        {
            freinD = true;
            FArD.freiner(freinD);
            freinG = false;
            FArG.freiner(freinG);
        }
        
        else if (Buggy.glissGlobal() < -20)
        {
            Buggy.freiner(0);
            Buggy.accelerer(0);
        }
        
        else if (Buggy.glissGlobal() > 20)
        {
                int diffSignal(Radio.signal());
                diffSignal = diffSignal-10;
                Moteur.accelerer(diffSignal);
        }
        
        else
        {
            Buggy.freiner(Radio.signal());
            Buggy.accelerer(Radio.signal());
        }
        delay(10);
     }
