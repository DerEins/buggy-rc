#ifndef FREIN_H
#define FREIN_H

#include <Arduino.h> // import de la librairie Arduino nécessaire pour la compatibilité avec la carte

class Frein // Classe (plan de construction) de l'objet Frein
{
public: // méthodes de l'objet Buggy
Frein();         // constructeur par dééfaut
Frein(int frein);         // constructeur avec choix du pin de commande du frein
~Frein();         // destructeur
void freiner(int brake);         // méthode permettant d'actionner le frein

private: // attrubuts propres à l'objet Buggy
int m_frein;         // pin de commande du frein

};

#endif
