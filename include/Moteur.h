#ifndef MOTEUR_H
#define MOTEUR_H

#include <Arduino.h>
#include <Servo.h>// import de la librairie Arduino nécessaire pour la compatibilité avec la carte

class Moteur // Classe (plan de construction) de l'objet Moteur
{
public: //méthodes de l'objet Moteur
Moteur();         //constructeur par défaut
Moteur(int moteur);         //constructeur avec choix du pin de commande du moteur
~Moteur();         // destructeur
void accelerer(int speed);         //méthode permettant d'actionner le moteur en fonction du signal choisi (v defaut = 0)

private: // attrubuts propre à l'objet Moteur
int m_moteur;         // pin de commande du moteur
Servo m_servoesc;

};

#endif
