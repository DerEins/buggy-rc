#ifndef BUGGY_H
#define BUGGY_H

#include <Arduino.h> // import des librairies et des autres classes d'objet nécessaires à l'objet
#include "Roue.h"
#include "Frein.h"
#include "Moteur.h"

class Buggy // Classe (plan de construction) de l'objet Buggy
{
public: // méthodes de l'objet Buggy

Buggy();         // constructeur par défault
Buggy(int tGlissGlobal, int tGlissRel, Roue RAvG, Roue RAvD,
      Roue RArG, Roue RArD, Frein FArG, Frein FArD,Frein Both, /*Frein FMoteur*/ Moteur Moteur);   // constructeur personnalisable
~Buggy();         // destructeur
int vitesse();         // méthode calculant de la vitesse de référence du buggy
int glissGlobal();         // méthode calculant le glissement global du buggy
int glissRel();         // méthode calculant le glissement relatif du l'essieu arrière du buggy
void freiner(int signal);         // méthode permettant de freiner le buggy
void accelerer(int signal);         // méthode permettant d'accélérer le buggy
int signal();                       //méthode récupérant le signal radio

private: // attributs propres au buggy crée

int m_tGlissGlobal;         // Tolérance du glissement global
int m_tGlissRel;         //  Tolérance du glissement relatif
int m_signal;
Roue m_RAvG;         // Roue avant-gauche
Roue m_RAvD;         // Roue avant-droite
Roue m_RArG;         // Roue arrière-gauche
Roue m_RArD;         // Roue arrière-droite
Frein m_FArG;         // Frein arrière-gauche
Frein m_FArD;
Frein m_Both;// Frein arrière-droit
//Frein m_FMoteur; frein moteur, inutilisé
Moteur m_moteur;         // Moteur du Buggy

};

#endif
