#ifndef ROUE_H
#define ROUE_H

#include <Arduino.h> // import de la librairie Arduino nécessaire pour la compatibilité avec la carte

class Roue // Classe (plan de construction) de l'objet Roue
{
public: // méthodes de l'objet Roue
Roue();         // constructeur par défaut
Roue(int capteur);         // constructeur avec choix du pin du capteur de la roue
~Roue();         // destructeur
int vitesse();         // méthode permettant de connaitre la vitesse d'une roue

private: // attrubuts propres à l'objet Roue
int m_capteur;         //pin du capteur de la roue

};

#endif
