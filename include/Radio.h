#ifndef RADIO_H
#define RADIO_H

#include <Arduino.h> // import de la librairie Arduino nécessaire pour la compatibilité avec la carte

class Radio // Classe (plan de construction) de l'objet Radio
{
public: //méthodes de l'objet Radio
Radio();         // constructeur par défaut
Radio(int radio);         //constructeur avec choix du pin du module radio
~Radio();         // destructeur
int signal();         // méthode permettant de récupérer le signal envoyé par le module radio

private: // attributs propre à l'objet Radio

int m_radio;         // pin du module radio

};

#endif
