# Bienvenue au projet "Buggy RC" du Lycée Pierre Joël Bonté

Dans la cadre des épreuves de Projets de l'année 2018-2019, il nous a été soumis un projet d'optimisation d'un Buggy Rc éléctrique à l'échelle 1:5. Ce Buggy possède de très bonnes performance en vitesse mais il est victime de patinage au freinage et à l'accélération, mais aussi d'un patinage relatif entre les roues arrières. Notre objectif est donc d'équiper ce véhicule d'un système d'antipatinage global et relatif ainsi que d'un ABS. 
