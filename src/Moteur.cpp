#include "../include/Moteur.h" // import de l'en-tête de la classe de l'objet

#include <Arduino.h>
#include <Servo.h>// import de la libraire Arduino nécessaire à la compatibilité avec la carte.


Moteur::Moteur() : m_moteur(2), m_servoesc()
{
        // construction avec les valeur par défaut de l'objet Moteur
}

Moteur::Moteur(int moteur) : m_moteur(moteur), m_servoesc()
{
        // construction avec choix du pin de commande du moteur
}

Moteur::~Moteur()
{
        // destructeur
}

void Moteur::accelerer(int speed)
{
        m_servoesc.attach(m_moteur);
        m_servoesc.write(map(speed,0,255,0,180));// création d'un signal PWM (MLI) dans le pin de commande pour contrôler la puissance du moteur en fonction du signal voulu
}

