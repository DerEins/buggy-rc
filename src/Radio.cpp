#include "../include/Radio.h" // import de l'en-tête de la classe de l'objet

#include <Arduino.h> // import de la libraire Arduino nécessaire à la compatibilité avec la carte.

Radio::Radio() : m_radio(A0)
{
        // construction avec les valeur par défaut de l'objet Radio
}

Radio::Radio(int radio) : m_radio(radio)
{
        // construction avec choix du pin de commande du moteur
}

Radio::~Radio()
{
        // destructeur
}

int Radio::signal() // récupération du signal envoyé par le modure radio
{
        int signal(analogRead(m_radio)); // Création de la variable récupérant la valeur de la tension aux bornes du pin du module radio
        return signal; // renvoi de la valeur récupérée précédemment

        /*Discussion à envisager sur la possibilité de conversion du signal car nécesité d'inverser le signal avant
           un transistor 3.3 V > 36 V */
}
