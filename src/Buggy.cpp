#include "../include/Buggy.h" // import de l'en-tête de l'objet Buggy

#include <Arduino.h> // import des librairies et des objets nécessaires à la création de l'objet
#include "../include/Roue.h"
#include "../include/Frein.h"
#include "../include/Moteur.h"



Buggy::Buggy() : m_tGlissGlobal(20), m_tGlissRel(20), m_RAvG(4), m_RAvD(5),
        m_RArG(6), m_RArD(7), m_FArG(0), m_FArD(1),m_Both(3), /*m_FMoteur(6)*/ m_moteur(2)
{
        // constructeur avec les valeurs par défaut
}

Buggy::Buggy(int tGlissGlobal, int tGlissRel, Roue RAvG, Roue RAvD, Roue RArG, Roue RArD, Frein FArG, Frein FArD, Frein Both, /*Frein FMoteur*/ Moteur Moteur) : m_tGlissGlobal(tGlissGlobal), m_tGlissRel(tGlissRel), m_RAvG(RAvG), m_RAvD(RAvD),
        m_RArG(RArG), m_RArD(RArD),m_FArG(FArG), m_FArD(FArD),m_Both(Both), /*m_FMoteur(FMoteur)*/ m_moteur(Moteur)
{
        // constructeur avec les choix des objets par l'utilisateur
}

Buggy::~Buggy()
{
        // destructeur
}

int Buggy::glissGlobal() // calcul du glissement global
{
        int glissement(100*(1-((m_RAvD.vitesse()+m_RAvG.vitesse())/(m_RArD.vitesse()+m_RArG.vitesse()))));
        /*
           On multiplie par 100 pour avoir un pourcentage et pour travailler avec des valeurs entières, plus rapides à calculer.
         */
        return glissement; // on renvoir la valeur du glissement global
}

int Buggy::glissRel() // calcul du glissement relatif
{
        int glissement(m_RArD.vitesse()*100/m_RArG.vitesse());
        /*
           On multiplie par 100 pour avoir un pourcentage et pour travailler avec des valeurs entières, plus rapides à calculer.
         */
        return glissement; // on renvoir la valeur du glissement relatif
}

void Buggy::freiner(int brake) // commande du freinage général du buggy
{
        m_FArD.freiner(brake); // freinage de la roue arrière-droite en fonction du signal voulu
        m_FArG.freiner(brake);
        m_Both.freiner(brake);// freinage de la roue arrière-gauche
        /*On doit voir ici comment on peut freiner les deux freins en même temps pour éviter les sorties de trajectoire
           La création d'un frein virtuel qui freinerait les deux freins en même  temps grâce à un pin commun est à envisager*/
}


void Buggy::accelerer(int speed) // commande de l'accélération général du buggy
{
        m_moteur.accelerer(speed); // commande d'acélération du moteur en fonction du signal voulu
}
