#include "../include/Roue.h" // import de l'en-tête de la classe de l'objet

#include <Arduino.h> // import de la libraire Arduino nécessaire à la compatibilité avec la carte.

volatile int rpmcount(0);  //variable volatile comptant le nombre de chute du signal tous les 100 ms

void rpm_fan() // compteur de descence de signal dans le pin du capteur à effet Hall
{
        rpmcount++; // incrémentation de la variable comptant le nombre de chute du signal tous les 100 ms
}

Roue::Roue() : m_capteur(0)
{
        // construction avec les valeur par défaut de l'objet Radio
}

Roue::Roue(int capteur) : m_capteur(capteur)
{
        // construction avec choix du pin du capteur à effet Hall
}

Roue::~Roue()
{
        // destructeur
}

int Roue::vitesse() // mesure de la vitesse de la roue
{
        int rpm(0); // initialisation de la variable contenant la vitesse de la roue
        rpmcount=0; // (ré)initialisation de la variable comptant le nombre de chute du signal tous les 100 ms

        attachInterrupt(m_capteur, rpm_fan, FALLING); // création d'une interruption dans le pin du capteur pendant 100 ms permettant de compter la fréquence du signal
        delay(100); // délai de 100 ms pour avoir un certain nombre de mesure
        detachInterrupt(m_capteur); //fin de l'interuption du signal pendant le calcul de la fréquence de rotation
        rpm = rpmcount * 600 * 2;
        /* Conversion de la fréquence du signal en fréquence de rotation (tr/min).
           Multiplé par 2 car 2 aiments par disque et par 10 car on ne mesure qu'1/10 de secondes*/
        return rpm; // renvoi de la vitesse de la roue en tr/min
}
