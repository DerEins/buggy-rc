#include "../include/Frein.h" // import de l'en-tête de la classe de l'objet

#include <Arduino.h>
// import de la libraire Arduino nécessaire à la compatibilité avec la carte.

Frein::Frein() : m_frein(3)
{
        // construction avec la valeur par défaut de l'objet
}

Frein::Frein(int frein) : m_frein(frein)
{
        // constructin avec choix du pin de commande du frein
}

Frein::~Frein()
{
        // destructeur
}

void Frein::freiner(int brake) // commande de freinage
{
        
            analogWrite(m_frein, brake);
        
        
        
         // création d'un signal PWM (MLI) dans le pin de command pour contrôler la force de freinage en fonction du signal voulu
}
